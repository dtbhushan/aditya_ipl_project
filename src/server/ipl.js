function matchesPerYear() {
    const fileReader = require("fs");
    matchObject = fileReader.readFileSync("/home/aditya/dataproject/src/data/matches.csv")
    var matchArray = matchObject.toString().split("\n");
    let headers = matchArray[0].split(",")
    yearAndMatches = {}
    for (let index = 1; index < matchArray.length; index++) {
        row = matchArray[index].split(",")
        if (yearAndMatches.hasOwnProperty(row[1]) == true) {
            yearAndMatches[row[1]] += 1
        }
        else {
            yearAndMatches[row[1]] = 1
        }
    }
    var json = JSON.stringify(yearAndMatches);
    fileReader.writeFile('src/public/output/matchesPerYear.json', json, 'utf8', (err) => {
        if (err) { console.error(err); return; };
        console.log("File has been created");
    });
}

function matchesPerTeamPerYear() {
    const fileReader = require("fs");
    matchObject = fileReader.readFileSync("/home/aditya/dataproject/src/data/matches.csv")
    var matchArray = matchObject.toString().split("\n");
    let headers = matchArray[0].split(",")
    matchesWonPerTeamPerYear = {}
    for (index = 1; index < matchArray.length; index++) {
        row = matchArray[index].split(",")
        if (matchesWonPerTeamPerYear.hasOwnProperty(row[10])) {
            if (matchesWonPerTeamPerYear[row[10]].hasOwnProperty(row[1])) {
                matchesWonPerTeamPerYear[row[10]][row[1]] += 1
            }
            else {
                matchesWonPerTeamPerYear[row[10]][row[1]] = 1
            }
        }
        else {
            year = row[1];
            matchesWonPerTeamPerYear[row[10]] = { year: 1 }
        }
    }
    var json = JSON.stringify(matchesWonPerTeamPerYear);
    fileReader.writeFile('src/public/output/matchesPerTeamPerYear.json', json, 'utf8', (err) => {
        if (err) { console.error(err); return; };
        console.log("File has been created");
    });
    
}


function extraRuns() {
    const fileReader = require("fs");
    matchObject = fileReader.readFileSync("/home/aditya/dataproject/src/data/matches.csv")
    deliveriesObject = fileReader.readFileSync("/home/aditya/dataproject/src/data/deliveries.csv")
    let deliveriesArray = deliveriesObject.toString().split("\n");
    let matchArray = matchObject.toString().split("\n");
    let headers = matchArray[0].split(",")

    var arrayOfMatchIds = []
    var teamsAndExtraRuns = {}
    for (index = 1; index < matchArray.length; index++) {
        row = matchArray[index].split(",")
        if (row[1] == 2016) {
            arrayOfMatchIds.push(row[0])
        }
    }

    for (index = 1; index < deliveriesArray.length; index++) {
        row = deliveriesArray[index].split(",")
        if (arrayOfMatchIds.includes(row[0]) == true) {
            if (teamsAndExtraRuns.hasOwnProperty(row[3]) == true) {
                teamsAndExtraRuns[row[3]] += parseInt(row[16])
            }
            else {
                teamsAndExtraRuns[row[3]] = parseInt(row[16])
            }
        }
    }
    var json = JSON.stringify(teamsAndExtraRuns);
    fileReader.writeFile('src/public/output/teamsAndExtraRuns.json', json, 'utf8', (err) => {
        if (err) { console.error(err); return; };
        console.log("File has been created");
    });
    

}

function economicalBowler() {
    const fileReader = require("fs");
    matchObject = fileReader.readFileSync("/home/aditya/dataproject/src/data/matches.csv")
    deliveriesObject = fileReader.readFileSync("/home/aditya/dataproject/src/data/deliveries.csv")
    let deliveriesArray = deliveriesObject.toString().split("\n");
    let matchArray = matchObject.toString().split("\n");
    let arrayOfMatchIds = []
    let bowlerAndRunsBalls = {}
    let bowlerAndEconomy = {}
    let topTenEconomicalBowlers = []
    for (index = 1; index < matchArray.length; index++) {
        row = matchArray[index].split(",")
        if (row[1] == 2015) {
            arrayOfMatchIds.push(parseInt(row[0]))
        }
    }


    for (index = 1; index < deliveriesArray.length; index++) {
        ball = deliveriesArray[index].split(",")
        bowler = ball[8]
        totalRuns = parseInt(ball[17])
        if (bowlerAndRunsBalls.hasOwnProperty(bowler) == true) {
            bowlerAndRunsBalls[bowler][0] += 1;
            bowlerAndRunsBalls[bowler][1] += totalRuns;
        }
        else {
            bowlerAndRunsBalls[bowler] = [1, totalRuns]
        }
    }
    bowlers = Object.keys(bowlerAndRunsBalls)
    for (index = 0; index < bowlers.length; index++) {
        if (bowlerAndEconomy[bowlers[index]] !== 'undefined') {
            bowlerAndEconomy[bowlers[index]] = bowlerAndRunsBalls[bowlers[index]][1] / bowlerAndRunsBalls[bowlers[index]][0] * 6;
        }
    }

    delete bowlerAndEconomy.undefined


    const sorted = Object.entries(bowlerAndEconomy)
        .sort(([, v1], [, v2]) => v1 - v2)
        .reduce((obj, [k, v]) => ({
            ...obj,
            [k]: v
        }), {})


    topTenEconomicalBowlers = Object.keys(sorted).slice(0, 10)

    var json = JSON.stringify(topTenEconomicalBowlers);
    fileReader.writeFile('src/public/output/topTenEconomicalBowlers.json', json, 'utf8', (err) => {
        if (err) { console.error(err); return; };
        console.log("File has been created");
    });
}
matchesPerYear()
matchesPerTeamPerYear()
extraRuns()
economicalBowler()